#include "main.hpp"
using namespace std;
void losuj(vector <string> &vec, int ile, int count)
{
    int i, j;
    string str;
    char c;
    for(i=0; i<ile; i++)
    {
        for (j=0; j<count; j++)
        {
            c = rand() % 26 + 'A';
            str += c;
        }
        vec.push_back(str);
        str.clear();
    }
}
int draw_menu(std::vector<std::string> menu) {
    erase();
    printw("Select menu entry \n\n");
    for (int i = 0; i < menu.size(); i++) {
        printw("  %s\n", menu[i].c_str());
    }
    int x,y, ch, cur_elm = 0;
    curs_set(0);
    getyx(stdscr, y, x);
    x+=2;
    while (ch != 'q') {
        ch = getch();
        switch(ch) {
        case KEY_DOWN:
            if (cur_elm < menu.size()-1)
                cur_elm++;
                mvaddch(x+cur_elm,0, '>');
                mvaddch(x+cur_elm-1, 0, ' ');

            break;
        case KEY_UP:
            if (cur_elm > 0)
                cur_elm--;
                mvaddch(x+cur_elm,0, '>');
                mvaddch(x+cur_elm+1, 0, ' ');
            break;
        case '\n':
            /*
            int _x, _y;
            getyx(stdscr, _y,_x);
            move(40, 0);
            move(_x, _y)*/;
            return cur_elm;
        }

        refresh();
    }
    return 0;
}
