
#include <ncurses.h>
#include <vector>
#include <string>
void losuj(std::vector <std::string> &vec, int ile, int count);
int draw_menu(std::vector<std::string> vec);

enum class TileType {
    VERTICAL_WALL,
    HORIZONTAL_WALL,
    LEVEL_BORDER,
    DOOR,
    SPACE,
    PLAYER

};

struct Tile {
    int x, y;
    int prev_x, prev_y;
    TileType type;
};
std::vector<Tile> load_map(std::string filePath);
void handle_player(std::vector<Tile> map);
