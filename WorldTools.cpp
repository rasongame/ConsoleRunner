#include "main.hpp"
#include <fstream>
using TT = TileType;
bool can_go(int x, int y, std::vector<Tile> map) {
    for (auto tile : map) {
        if (tile.x == x && tile.y == y) {
            if (tile.type == TT::SPACE) {
                return true;
            } else { return false; }
        }
    }
    return false;
}
void update_map(std::vector<Tile> map) {
    clear();
    for (auto tile : map) {

        move(tile.y, tile.x);
        if (tile.type == TT::VERTICAL_WALL){
            addch('|');
        }else if (tile.type == TT::HORIZONTAL_WALL){
            addch('-');
        }else if (tile.type == TT::DOOR){
            addch('\\');
        }else if (tile.type == TT::SPACE){
            addch(' ');
        }else if (tile.type == TT::LEVEL_BORDER){
            addch('*');
        } else
        if (tile.type == TT::PLAYER) {
            addch('0');
        }
    }
    refresh();
}
std::vector<Tile> load_map(std::string filePath) {
    // draw map
    std::ifstream fin;
    fin.open(filePath, std::ios::in);
    char ch;
    int num_lines = 0;
    int num_ch = 0;
    clear();
    move(0,0);
    std::vector<Tile> tiles;
    // \ - дверь
    // | стена
    // - стена
    // * конец линии
    // Координата X - горизонтальная позиция
    // Координата Y - вертикальная
    while(!fin.eof()) {
        fin.get(ch);
        num_ch++;
        Tile tile;
        tile.x = num_ch;
        tile.y = num_lines;
        tile.prev_x = tile.x;
        tile.prev_y = tile.y;
        switch(ch) {
        case '0':
            addch('0');
            tile.type = TT::PLAYER;
            break;
        case '|':
            addch('|');
            tile.type = TT::VERTICAL_WALL;
            break;
        case '-':
            addch('-');
            tile.type = TT::HORIZONTAL_WALL;
            break;
        case '*':
            printw("\n");
            tile.type = TT::LEVEL_BORDER;
            num_lines++;
            num_ch = 0;
            break;
        case '\\':
            tile.type = TT::DOOR;
            printw("\\");
            break;
        case ' ':
            tile.type = TT::SPACE;
            addch(' ');
            break;
        }
        tiles.push_back(tile);
    }
    refresh();
    struct {
        int walls, doors, levels, space;
    } count_of;
    for (unsigned long i = 0; i < tiles.size(); i++) {
        switch(tiles[i].type) {
        case TT::HORIZONTAL_WALL:
        case TT::VERTICAL_WALL: {
            count_of.walls++;
        };
        case TT::LEVEL_BORDER: {
            count_of.levels++;
        };
        case TT::SPACE: {
            count_of.space++;
        };
        case TT::DOOR: {
            count_of.doors++;
        };
        default: {};
        }

    }
    printw("%d %d %d", count_of.walls, count_of.levels, count_of.doors);
    refresh();
    return tiles;
};

void handle_player(std::vector<Tile> map) {
    int ch = 0;
    Tile *player;

    for (unsigned long i = 0; i < map.size(); i++) {
        if (map[i].type == TT::PLAYER)
            player = &map[i];
    }

    while(ch != 'q') {
        ch = getch();
        switch(ch) {
        case KEY_LEFT: {
            if (can_go(player->x-1, player->y, map))
                player->x--;
            break;
        }
        case KEY_RIGHT: {
            if (can_go(player->x+1, player->y, map))
                player->x++;
            break;

            }
        case KEY_DOWN:
            if (can_go(player->x, player->y+1, map))
                player->y++;
            break;
        case KEY_UP:
            if (can_go(player->x, player->y-1, map))
                player->y--;
            break;
        }
        update_map(map);
    }
};
